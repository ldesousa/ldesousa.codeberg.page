# ldesousa.codeberg.page

[Personal web site](http://ldsousa.codeberg.page)

## Copyright

Copyright (c) 2016-2020 Luís Moreira de Sousa. All rights reserved. 
Any use of this software constitutes full acceptance of all terms of the 
document licence.

## Development

To generate the HTML pages and serve them locally use this command:

```
bundle exec jekyll serve
```

Then point your browser to: [http://localhost:4000](http://localhost:4000).

## Licence

These documents are published under the [EUPL 1.1 licence](https://joinup.ec.europa.eu/community/eupl/og_page/introduction-eupl-licence). 
For full details please consult the LICENCE file.
