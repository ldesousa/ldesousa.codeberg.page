---
layout: default
---

Hexagons
=========

Programming tools and algorithms for hexagonal rasters was the first thing I ever did resembling research. It was the topic of my MSc thesis, and while a MSc at the time was much closer to a modern day PhD, hexagons took me well beyond that. An application to hydrography resulted in one of my first peer-reviewed publications, that still gets cited now an then.

During my stint at EAWAG n Switzerland I applied hexagonal rasters yet again, then to flood modelling. A new suite of tools and accompanying literature emerged from that work that has kept evolving since. Further tools and publications remain in the offing, much to the drive of the [Urban Water Systems department](https://www.eawag.ch/en/department/sww/) at EAWAG.

<img src="QGis_all.png"
     alt="Markdown Monster icon"
     style="width: 100%;" />

The presentation below outlines some of the peculiarities and advantages of hexagons that make them so fascinating.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uLO4HDCVBp0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Beyond their advantages in tiling the Euclidian space, hexagons are also a useful means to partition the rounded surface of the Earth. I keep interest open on this specific topic through the application of Discrete Global Grid Systems (DGGS) to raster modelling.


Tools
-----

- [HexASCII](https://gitlab.com/ldesousa/HexAsciiBNF) - specification of a file format for hexagonal rasters.

- [hex-utils](https://codeberg.org/ldesousa/hex-utils) - tools to create and use hexagonal rasters with the HexASCII format.

- [hex-utils-qgis](https://gitlab.com/ldesousa/hex-utils-qgis) - QGis plug-in to use HexASCII rasters.

- [hex-utils-grass](https://gitlab.com/ldesousa/hex-utils-grass) - GRASS plug-in for HExASCII rasters (unfinished).

Selected Literature
-------------------

- L. M. de Sousa, J. P. Leitão. Hex-utils: A Tool Set Supporting HexASCII Hexagonal rasters. *Proceedings of the 3rd International Conference on Geographical Information Systems Theory, Applications and Management: GISTAM*, 1,
177-183,	2017.

- L. M. de Sousa, J. P. Leitão. [HexASCII: A file format for cartographical hexagonal rasters](https://gitlab.com/ldesousa/ldesousa.gitlab.io/raw/master/documents/HexASCII_Specification.pdf). *Transactions in GIS*,	00:116, 2017

- J.P. Leitão, L. M. de Sousa, M. J. Gibson, A. S. Chen, D. A. Savić. Hexagonal cellular automata for flood modelling. *Proceedings of CAMUSS*, 2016.

- L. M. de Sousa, F. Nery, R. Sousa and J. Matos, [Grelhas Geodésicas Globais](http://ftp.igeo.pt/instituto/cegig/got/3_Docs/Files/Sousa_Sousa_Nery_Matos_2007_Grelhas.pdf), *V Congresso Nacional de Cartografia e Geodesia*, Abril 2007.

- L. M. de Sousa, F. Néry, R. Sousa and J. Matos, Assessing the accuracy of hexagonal versus square tilled grids in preserving DEM surface flow directions, *7th International Symposium on Spatial Accuracy Assessment in Natural Resources and Environmental Sciences*, July 2006.
