---
layout: default
---


Other interests
===============

Resource Depletion
------------------

Life isn't just work, though some hobbies can be taken as serious as work. In May of 2005 the ASPO conference took place in Lisbon; though not attending, the knowledge I acquired in the following days made me realise life as we knew it was over. The following decade much of my spare time was spent studying [resource depletion](depletion.html).


Hobbies
-------

To balance the sedentary life of research, I ride my [bike](cycling.html) whenever possible, covering several thousands Km every year.


[Music](/others/music.html) is an old passion. While no longer an active musician, I keep browsing the music universe as an avid listener.

Follow what I am currently reading at [GoodReads.com](https://www.goodreads.com/user/show/117114461-lu-s-de-sousa). I also post book reviews to Mastodon under the [BookReview hashtag](https://mastodon.social/@luis_de_sousa/tagged/BookReview).

