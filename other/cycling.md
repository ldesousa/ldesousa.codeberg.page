---
layout: default
---


Cycling
=======

Passionate spectator of the sport, I also cover thousands of kilometers every year on my bicycle. The Neterlands has great infrastructure for cycling but no mountains. I try to spend at least a week every summer in a mountainous place to taste all the suffering and joy a bicycle can offer.

Cycling is much more than sport, more will come on this topic at some point. For now my activity is available at [Strava](https://www.strava.com/athletes/20827733).
