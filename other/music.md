---
layout: default
---

Music
=====

I listen to an assorted mix of music styles that reflect various phases of a lifetime musical journey. However, for the past 15 years Progressive Rock has largely dominated my playlists. I am a patron of the [Aural Moon](http://auralmoon.com/) web radio.

My audiophile profile can be found at [Last.fm](http://www.last.fm/user/Luis_de_Sousa). Musical video suggestions are collected at [Pinterest](https://www.pinterest.com/lus7292/musical-suggestions/). I post album reviews to Mastadon under the hastag [AlbumReview](https://mastodon.social/@luis_de_sousa/tagged/AlbumReview).

Recommended music indexes:
- [ProgArchives](http://www.progarchives.com)
- [MetalMusicArchives](http://www.metalmusicarchives.com)
- [JazzMusicArchives](http://www.jazzmusicarchives.com/)
