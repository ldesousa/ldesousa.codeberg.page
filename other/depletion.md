---
layout: default
---


Resource Depletion
==================

During the summer of 2005 I started a Portuguese language web-site on Peak Oil ([PicoDoPetroleo.net](http://www.picodopetroleo.net/)) when faced with the wide apathy of local media and political class towards the subject. Later that year I  was invited to start the Portuguese chapter of ASPO, becoming the developer and manager of its web-site ([ASPO-Portugal.com](http://www.aspo-portugal.com/))

In 2006 I started submitting research articles to [TheOilDrum](http://www.theoildrum.com), an electronic forum on energy depletion that would become the world reference in this field. During the Fall of 2006 my first article on World Oil Exports was published, in which I postulated the volume of petroleum traded internationally had entered a terminal decline, being this the utmost factor behind rising prices in previous years. By the end of that year I would integrate the team that started the [European chapter](http://europe.theoildrum.com) of TheOilDrum, thus becoming a permanent contributor to the forum.

I deepened my work on Exports the following years. In October of 2008 I was an invited speaker to the ASPO Conference in Barcelona, where I presented a model encompassing 25 individual petroleum exporting nations, which produced essentially the same conclusions of the earlier assessment in 2006. Almost 5 years after my ground breaking observations there are now but a handful of countries in the world with real perspectives to increase petroleum exports; little doubt remains on the accuracy of the 2006 forecast.

Beyond petroleum exports, my research has touched several subjects through the years, including the Olduvai Theory, macro-economic consequences of Petroleum depletion and Coal supplies. After 2009 I focused on Energy Policy at the national and European scopes. I was regularly invited to address the subject of fossil fuel depletion publicly and regularly collaborated with the local press on the economics of energy and metals.

My most recent writings on resource depletion can be found in the blog [AtTheEdgeOfTime](http://attheedgeoftime.blogspot.ch/search/label/energy). This blog was mothballed in 2017, in the wake of health issues and Google's approach to privacy.

### Relevant articles:

[World Oil Exports](http://www.theoildrum.com/story/2006/10/5/215316/408)
 - [Methodology](http://europe.theoildrum.com/node/4179)
 - [Angola](http://europe.theoildrum.com/node/4184)
 - [Libya](http://europe.theoildrum.com/node/4513)

[A New Energy Policy for Europe](http://europe.theoildrum.com/node/2169)

[Dialoguing with Dr. Peter Jackson CERA: Is the Future of Oil Resources Secure?](http://europe.theoildrum.com/node/2283)

[From sweet on the table to fuel in the tank: the millenary history of Sugar Cane](http://europe.theoildrum.com/node/2390)

[Marchetti's Curves](http://europe.theoildrum.com/node/2746)

[A few more thoughts on Saudi and HL](http://europe.theoildrum.com/node/3100)

[Olduvai revisited 2008](http://europe.theoildrum.com/node/3565)

[IEA WEO 2008 - Fossil Fuel Ultimates and CO2 Emissions Scenarios](http://europe.theoildrum.com/node/4807)

[Energy Policy: SER-2 - part I](http://europe.theoildrum.com/node/4942)<br>
[Energy Policy: SER-2 - part II](http://europe.theoildrum.com/node/4967)<br>
[Energy Policy: SER-2 - part III](http://europe.theoildrum.com/node/5049)

[Planning for Europe's Energy Future](http://europe.theoildrum.com/node/6694)
