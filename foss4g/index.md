---
layout: default
---

FOSS4G
=========

Free and Open Source Software for Geo-Spatial -- FOSS4G for short -- is actually the name of an annual conference, but precisely captures the nature of the tools I use and those I develop. GRASS-GIS was my entry point to a world where software users are first class citizens, themselves involved in all stages of software development. Through the years I contributed code and documentation to many different projects: QGis, OpenLayers, GeoTools, OWSLib and more.

During my time at the [Luxembourg Institute of Science and Technology](http://www.list.lu) (LIST) I took part in the development of [iGUESS](https://iguess.list.lu/), a decision support system relying heavily on [PyWPS](http://www.pywps.org). From user to developer was one small step, in 2015 I integrated the core team that developed PyWPS-4. About that time I was appointed as charter member of the [OSGeo Foundation](http://www.osgeo.org) and soon after I was a founding member of the PyWPS Project Steering Committee. 

In 2018 I chaired the Scientific Committee of the [FOSS4G-Europe conference](https://foss4g-europe.osgeopt.pt), organised by the Portuguese chapter of OSGeo, of which I am a board member.

More recently I have worked on [didactic contents](https://github.com/geopython/geopython-workshop) for geo-spatial data processing with the Python programming language. 


Selected literature
-------------------

- V. Maquil, U. Leopold, L. M. de Sousa, L. Schwartz, & E. Tobias. [Towards a framework for geospatial tangible user interfaces in collaborative urban planning](http://search.proquest.com/openview/07fce75483c4b9e8584a161d07ab9739/1.pdf?pq-origsite=gscholar&cbl=46749&casa_token=Fg1d0e1aQOkAAAAA:oSAKCLSPTyfegKHPdhQ7J6OIvNB37Fo6ke1_ov-Atp5RW34hTAZ_cvbQ3evTWf9whRB5KfAtl1s). Journal of Geographical Systems, 20(2), 185-206, 2018.

- J. Čepický and L. M. de Sousa. [New implementation of OGC Web Processing Service in Python programming language](https://www.int-arch-photogramm-remote-sens-spatial-inf-sci.net/XLI-B7/927/2016/isprs-archives-XLI-B7-927-2016.pdf). *Int. Arch. Photogramm. Remote Sens. Spatial Inf. Sci.*, 927-930,	2016.


- V. Maquil, L. de Sousa, U. Leopold and E. Tobias. A geospatial tangible user interface to support stakeholder participation in urban planning. *I International Conference on Geographical Information Systems Theory, Applications and Management: GISTAM*, 2015.


- L. M. de Sousa, C. Eykamp , U. Leopold, O. Baume and C. Braun. iGUESS - A web based system integrating Urban Energy Planning and Assessment Modelling for multi-scale spatial decision making. *Procedings of the International Congress on Environmental Modelling and Software (iEMSs)*, Sixth Biennial Meeting. Leipzig, Germany, July 2012. 
