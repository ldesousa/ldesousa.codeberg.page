---
layout: default
---

Spatial Simulation
==================

I coded a few simple simulations during my early years as a junior researcher at [Instituto Superior Técnico](http://www.ist.pt). These simulations tested different hypotheses for environment scientists that could not themselves program. Sometimes the results were leaving my colleagues less than happy. This evolved into a broader discussion in the research group, how could we make simulation models more transparent to domain experts? Or even to allow them to develop these models with little to no programming? Eventually this became the research topic of my PhD thesis.

I chose to apply a Model-Driven Development (MDD) approach to this problem, making use of the infrastructures specified by the Object Management Group (OMG). The first outcome of this research was a graphical domain-specific language, baptised [DSL3S](https://github.com/MDDLingo/DSL3S). A set of plug-ins for the Eclipse IDE followed, that can transform a DSL3S model into a ready-to-run spatial simulation. With a relatively simple language it became possible to create spatial simulation models with different mechanics and application domains, without typing a single line of code.


<img src="DomainModel.png"
     alt="DSL3S Domain Model"
     style="align: center; width: 80%;" />

The video below provides an introduction to the DSL3S project. The [DSL3S wiki](https://github.com/MDDLingo/DSL3S/wiki) includes further information.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lTUtSDiG9ZQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Whereas DSL3S and its associated tools fulfilled the thesis goals, it never sparked the interest of the spatial simulation community. Modellers prefer to retain a finer control of their simulations, at the detriment of transparency and expressiveness.  However, the body of literature borne out of DSL3S has collected citations in recent years, harnessing interest among the MDD research community. 

Selected Literature
-------------------

- L. M. de Sousa and A. R. da Silva. Usability evaluation of the domain specific language for spatial simulation scenarios. *Cogent Engineering*, vol. 5, no 1, p. 1436889, 2018.

- L. M. de Sousa and A. R. da Silva. A Domain Specific Language for Spatial Simulation Scenarios. *GeoInformatica* 20 (1). Springer, 2016.


- A. Ribeiro, L. M. de Sousa, A. R. da Silva. Comparative Analysis of Workbenches to Support DSMLs: Discussion with Non-Trivial Model-Driven Development Needs. *MODELSWARD*, 323-330, 2016.


- B. Müller, S. Balbi, C.M. Buchmann, L. M. de Sousa, et al.. Standardised and
transparent model descriptions for agent-based models: Current status and prospects. *Environmental Modelling & Software*, 55. Elsevier, 2014.

- L. M. de Sousa and A. R. da Silva. Review of Spatial Simulation Tools for Geographic Information Systems. *SIMUL 2011: The Third International Conference on Advances in System Simulation*, Barcelona, Spain, October 2011.
