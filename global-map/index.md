---
layout: default
---

Global mapping
==============

Since 2017 I have worked at [ISRIC](http://www.isric.org) there tackling challenges in global mapping, within a team including some of the world's reference geo-statisticians and soil geographers. [SoilGrids](http://www.soilgrids.org) is ISRIC's flagship product, a series of consistent predictions of soil properties published as raster maps. SoilGrids is computed in a High Performance Computing cluster using state-of-the-art geo-statistics methods based on machine learning.

One of the questions I have been addressing is the quest for an appropriate coordinate system for global maps. Cartography tools developed in the age of computers are by and large oblivious to global mapping. Very few of the modern global cartographic projections developed in the past century are supported in common GIS software, making a project like SoilGrids all the more challenging. The Homolosine projection is a rare exception that currently provides the coordinate system for SoilGrids at prediction time. However, the presentation of global maps over the internet remains an open question.

Selected literature
-------------------

- L. M. de Sousa, L. Poggio, N. H. Batjes, et al. [SoilGrids 2.0: producing quality-assessed soil information for the globe](https://soil.copernicus.org/preprints/soil-2020-65/soil-2020-65.pdf). *SOIL Discussions*, p. 1-37, 2020.

- L. M. de Sousa, L. Poggio, G. Dawes, et al. [Computational Infrastructure of SoilGrids 2.0](https://www.researchgate.net/profile/Luis_De_Sousa4/publication/338887940_Computational_Infrastructure_of_SoilGrids_20/links/5e4a6c1092851c7f7f411588/Computational-Infrastructure-of-SoilGrids-20.pdf). In : *International Symposium on Environmental Software Systems*. Springer, Cham, p. 24-31, 2020.

- K. Ivushkin, H. Bartholomeus, A. K. Bregt, A. Pulatov, B. Kempen & L.M. de Sousa. [Global mapping of soil salinity change](https://www.sciencedirect.com/science/article/pii/S0034425719302792?casa_token=68i_PwQy2JwAAAAA:C2KJykjhsmcwly-3mCNZ5_FC8eBqjjxQ9H5NsUTkXg4YTGxIZClMUDJzcp-sAkZtHt1J9xzOe20). *Remote Sensing of Environment*, 231, 111260, 2019.

- L. M. de Sousa, L. Poggio and B. Kempen. [Comparison of FOSS4G supported equal-area projections using discrete distortion indicatrices](https://www.mdpi.com/2220-9964/8/8/351/htm). *ISPRS International Journal of Geo-Information*, vol. 8, no 8, p. 351, 2019.
